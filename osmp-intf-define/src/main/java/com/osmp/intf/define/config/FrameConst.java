/*   
 * Project: OSMP
 * FileName: FrameConst.java
 * version: V1.0
 */
package com.osmp.intf.define.config;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2016年8月9日 上午9:25:30上午10:51:30
 */
public class FrameConst {
	/***************************** 对外接口 **************************************/
	/**
	 * 接口source参数属性名
	 */
	public final static String SOURCE_NAME = "source";
	/**
	 * 接口传入服务参数属性名
	 */
	public final static String PARAMETER_NAME = "parameter";

	/***************************** 访问客户端解析 **********************************/
	/**
	 * 访问客户端ip属性名
	 */
	public final static String CLIENT_IP = "ip";

	/**
	 * 访问客户端项目属性名
	 */
	public final static String CLIENT_FROM = "from";
	/**
	 * 访问客户端代理属性名
	 */
	public final static String CLIENT_AGENT = "agent";
	/**
	 * 访问请求唯一标识属性名
	 */
	public final static String CLIENT_REQ_ID = "requestId";

	/**************************************************************************/
	/***************************** 服务常量 **************************************/
	/************************************************************************/
	/**
	 * 区分服务的属性键名
	 */
	public final static String SERVICE_NAME = "name";
	/**
	 * 服务描述
	 */
	public final static String SERVICE_MARK = "mark";

	/***************************************************************************/
	/******************************** 服务器信息 ***********************************/
	/**************************************************************************/

	/**
	 * 获取服务器ip地址
	 * 
	 * @return
	 */
	public static final String getLoadIp() {
		String localip = null;// 本地IP，如果没有配置外网IP则返回它
		String netip = null;// 外网IP
		try {
			Enumeration<NetworkInterface> netInterfaces = NetworkInterface
					.getNetworkInterfaces();
			InetAddress ip = null;
			boolean finded = false;// 是否找到外网IP
			while (netInterfaces.hasMoreElements() && !finded) {
				NetworkInterface ni = netInterfaces.nextElement();
				Enumeration<InetAddress> address = ni.getInetAddresses();
				while (address.hasMoreElements()) {
					ip = address.nextElement();
					if (!ip.isSiteLocalAddress() && !ip.isLoopbackAddress()
							&& ip.getHostAddress().indexOf(":") == -1) {// 外网IP
						netip = ip.getHostAddress();
						finded = true;
						break;
					} else if (ip.isSiteLocalAddress()
							&& !ip.isLoopbackAddress()
							&& ip.getHostAddress().indexOf(":") == -1) {// 内网IP
						localip = ip.getHostAddress();
					}
				}
			}
		} catch (SocketException e) {
			e.printStackTrace();
			return "";
		}
		if (netip != null && !"".equals(netip)) {
			return netip;
		} else {
			return localip;
		}
	}

}
