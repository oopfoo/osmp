/*   
 * Project: 基础组件 
 * FileName: SicentTestServiceImpl.java
 * Company: Chengdu Sicent Technology Co.,Ltd
 * version: V1.0
 */
package com.osmp.demo.interceptor;

import org.springframework.stereotype.Component;

import com.osmp.intf.define.config.FrameConst;
import com.osmp.intf.define.interceptor.ServiceInterceptor;
import com.osmp.intf.define.interceptor.ServiceInvocation;
import com.osmp.intf.define.model.ServiceContext;


/**
 * Description:日志拦截用于统计日志访问提供支撑数据
 * 
 * @author: wangkaiping
 * @date: 2014年9月26日 下午3:03:55
 */
@Component
public class OsmpDemoInterceptor implements ServiceInterceptor {


	@Override
	public Object invock(ServiceInvocation invocation) {
		ServiceContext context = invocation.getContext();
		String ip = context.getClientInfo().get(FrameConst.CLIENT_IP);
		String serviceName = context.getServiceName();
		System.out.println("serviceName ： " + serviceName);
		System.out.println("IP拦截 ： " + ip);
		if ("192.168.1.1".equals(ip)) {
			return "IP非法！";
		} else {
			return invocation.process();
		}
	}
}
