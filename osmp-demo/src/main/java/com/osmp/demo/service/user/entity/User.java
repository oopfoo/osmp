 /*   
 * Project: OSMP
 * FileName: User.java
 * version: V1.0
 */
package com.osmp.demo.service.user.entity;

import java.io.Serializable;

import com.osmp.jdbc.define.Column;

/**
 * Description:
 * @author: wangkaiping
 * @date: 2016年8月25日 下午8:32:47上午10:51:30
 */
public class User implements Serializable{
	private static final long serialVersionUID = -5557601915219524663L;
	
	@Column(mapField="id",name="id")
	private String id;
	@Column(mapField="name",name="name")
	private String name;
	@Column(mapField="age",name="age")
	private int age;
	@Column(mapField="remark",name="remark")
	private String remark;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
