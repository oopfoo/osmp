/*   
 * Project: OSMP
 * FileName: UserService.java
 * version: V1.0
 */
package com.osmp.demo.service.user;

import java.util.List;
import java.util.Map;

import com.osmp.demo.service.user.entity.User;
import com.osmp.jdbc.define.Page;

/**
 * Description:
 * 
 * @author: wangkaiping
 * @date: 2014年11月28日 下午5:27:33
 */
public interface UserService {

	public String getUserName(String name);

	public int getUserAge(int age);
	
	public int cudUser(Map<String,Object> map);
	
	public List<User> queryList(Map<String, Object> map);
	
	public Page<User> queryListByPage(Map<String, Object> map);

}
